/*
@author: ZZH
@date: 2021-12-12
@time: 10:21:38
@info: zCore 指令集
*/
#pragma once
#include "zCore.h"
#include <stdint.h>

typedef uint8_t(*InstHanlder_t)(void);

typedef enum
{
    //调用
    INST_CALL,

    //数据存取
    INST_PUSH,
    INST_POP,
    INST_LDW,
    INST_LDH,
    INST_LDB,
    INST_STW,
    INST_STH,
    INST_STB,

    //寄存器数据存取
    INST_LRL,//加载立即数到寄存器低位,最大值0x3FFFFFF
    INST_LRH,//加载立即数到寄存器高位,最大值0xFC
    INST_MOV,//寄存器间赋值

    //数学运算
    INST_ADD,
    INST_SUB,
    INST_MUL,
    INST_DIV,
    INST_MOD,

    //逻辑运算
    INST_AND,
    INST_OR,
    INST_NOT,
    INST_XOR,
    INST_EQ,//等于判断,会影响标志位C
    INST_GT,//大于判断,会影响标志位C
    INST_LT,//小于判断,会影响标志位C

    //位运算
    INST_BAND,
    INST_BOR,
    INST_BNOT,
    INST_BXOR,

    //跳转与条件跳转
    INST_J,//无条件跳转,会改变链接寄存器的值
    INST_JT,//PSR寄存器C位为1跳转,符合条件才会改变链接寄存器的值
    INST_JF,//PSR寄存器C位为0跳转,符合条件才会改变链接寄存器的值
    INST_RET,//返回的过程便是将链接寄存器的值赋值给PC
}enInst_t;

/*调用*/
//call
typedef struct
{
    uint32_t srvNumber : 16;//段内调用号
    uint32_t segNum : 10;//段号
    uint32_t inst : 6;//指令
}InstCall_t, * pInstCall_t;

//RET无操作数,也无寄存器
typedef struct
{
    enInst_t : 26;
    enInst_t inst : 6;
}Inst_t, * pInst_t;

/*寄存器数据存取*/
//LRL LRH
typedef struct
{
    uint32_t data : 16;//数据
    uint32_t : 6;
    uint32_t regNum : 4;//寄存器编号  0-15
    uint32_t inst : 6;//指令
}InstLrx_t, * pInstLrx_t;
#define LRL_DATA_MASK (0x0000FFFF)
#define LRH_DATA_MASK (0xFFFF0000)


/*寻址*/
//PUSH POP LD(W, H, B) ST(W, H, B)
typedef struct
{
    uint32_t offset : 18;//数据所在的偏移
    uint32_t srcReg : 4;//寻址所用的寄存器
    uint32_t dstReg : 4;//目标寄存器
    uint32_t inst : 6;//指令
}InstMem_t, * pInstMem_t;

/*寄存器数据存取*/
//MOV

/*基本运算*/
//+ - * / %

/*逻辑运算*/
//and or not xor

/*位运算*/
//& | ! ^

typedef struct
{
    uint32_t srcReg0 : 8;//寄存器编号  0-15
    uint32_t srcReg1 : 8;//寄存器编号  0-15
    uint32_t dstReg : 8;//寄存器编号  0-15
    uint32_t : 2;//无效位
    uint32_t inst : 6;//指令
}InstCalc_t, * pInstCalc_t;

/*跳转*/
//j-jump jt-jump if true jf-jump if false
typedef struct
{
    uint32_t offset : 25;//跳转目标位置的相对偏移,单位为指令数,每一条指令为4字节
    uint32_t sign : 1;//符号位
    uint32_t inst : 6;//指令
}InstJmp_t, * pInstJmp_t;

extern InstHanlder_t InstHandler[INST_RET + 1];

//调用
uint8_t InstCallHandler();

//数据存取
uint8_t InstPushHandler();
uint8_t InstPopHandler();
uint8_t InstLdwHandler();
uint8_t InstLdhHandler();
uint8_t InstLdbHandler();
uint8_t InstStwHandler();
uint8_t InstSthHandler();
uint8_t InstStbHandler();

//寄存器数据存取
uint8_t InstLrlHandler();
uint8_t InstLrhHandler();
uint8_t InstMovHandler();

//数学运算
uint8_t InstAddHandler();
uint8_t InstSubHandler();
uint8_t InstMulHandler();
uint8_t InstDivHandler();
uint8_t InstModHandler();

//逻辑运算
uint8_t InstAndHandler();
uint8_t InstOrHandler();
uint8_t InstNotHandler();
uint8_t InstXorHandler();
uint8_t InstEQHandler();
uint8_t InstGTHandler();
uint8_t InstLTHandler();

//位运算
uint8_t InstBAndHandler();
uint8_t InstBOrHandler();
uint8_t InstBNotHandler();
uint8_t InstBXorHandler();

//跳转
uint8_t InstJHandler();
uint8_t InstJTHandler();
uint8_t InstJFHandler();
uint8_t InstRetHandler();
