CC = gcc
CFLAGS = -g3 -Wall -o0 -std=gnu99

CXX = g++
CXXFLAGS = -g3 -Wall -o0 -std=gnu++11

LDFLAGS = 

OBJ =

TARGET = main.exe
RUN_ARGS = 
OBJS = 
HEADERS = 

export OBJ HEADERS

-include subdir.mk

run : all
	@echo StartRunning: $(TARGET) with arg: $(RUN_ARGS)
	@./$(TARGET) $(RUN_ARGS)

all : $(TARGET)

%.o : %.cpp Makefile
	@echo Building File: $<
	@$(CXX) $(CXXFLAGS) $(HEADERS) -c -o $@ $<

%.o : %.c %.h Makefile
	@echo Building File: $<
	@$(CC) $(CFLAGS) $(HEADERS) -c -o $@ $<

$(TARGET) : $(OBJS)
	@echo StartBuild: $(TARGET)
	@$(CXX) $(LDFLAGS) -o $@ $^

clean:
	@rm -f $(TARGET) main.o $(OBJS)

.PHONY : all clean run

