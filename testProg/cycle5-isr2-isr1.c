
//循环触发五次isr[2],然后触发isr[1]
uint32_t insts[] = {
    0X48000005,//LRL r0, 5   r0 = 5;
    0X4A000000,//LRL r1, 0   r1 = 0;
    0X4C000001,//LRL r2, 1   r2 = 1;
    0XB0000001,//GT r0, r1   if (r0 > r1) PSR.C = 1; else PSR.C = 0;
    0XF0000004,//JF 4        if(PSR.C == 0) PC +=4;
    0X68000002,//SUB r0, r2  r0 = r0 - r2;
    0X200,     //CALL 2      SVC_Table[2]();
    0XE4000004,//J -4        PC -= 4;
    0X100,     //CALL 1      SVC_Table[1]();
    0,         //Exception
};
