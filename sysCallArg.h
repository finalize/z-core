#pragma once
#include <stdlib.h>
typedef void* sysCallArg_t;
// #define sys_arg(parg, type) *((type*)parg); parg+=sizeof(type);
#define sys_arg(parg, type) ({type tmp = *(type*)parg; parg+=sizeof(type); tmp;})
