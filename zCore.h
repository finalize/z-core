/*
@author: ZZH
@date: 2021-12-12
@time: 10:58:39
@info:
*/
#pragma once
#include "sysCallArg.h"
#include <stdint.h>
#include <stdlib.h>

typedef void (*SysCall_t)(sysCallArg_t);
typedef struct
{
    size_t R[16];//内核寄存器 r0-r11, ds, sp, pc, lr

    union
    {
        struct
        {
            uint32_t C : 1;//条件位,影响条件跳转指令
            uint32_t Sup : 1;//权限位，标志处理器当前的权限级别
        };
        uint32_t Value;
    }PSR;//程序状态寄存器
    
    SysCall_t SVC_Table[8];//系统调用表(软中断向量表)
}ZCoreReg_t, * pZCoreReg_t;

extern ZCoreReg_t CoreReg;

#define INDEX_DS 12
#define INDEX_SP 13
#define INDEX_PC 14
#define INDEX_LR 15

#define CURRENT_PC (CoreReg.R[INDEX_PC])
#define CURRENT_INST(type, name) type name = (type) CURRENT_PC

#define CURRENT_SP (CoreReg.R[INDEX_SP])
#define CURRENT_LR (CoreReg.R[INDEX_LR])
#define CURRENT_DS (CoreReg.R[INDEX_DS])
