#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "inst.h"
#include "sysCallArg.h"
#include "zCore.h"
ZCoreReg_t CoreReg;
SysCall_t SysCallTable[8];

void printCoreReg(sysCallArg_t arg)
{
    //通用寄存器
    for (int i = 0;i < 12;i++)
        printf("R%d:%ld\r\n", i, CoreReg.R[i]);

    //控制寄存器
    printf("PSR:%#X\r\n", CoreReg.PSR.Value);
    printf("PC:%p\r\n", (void*) CURRENT_PC);
    printf("SP:%p\r\n", (void*) CURRENT_SP);
    printf("DS:%p\r\n", (void*) CURRENT_DS);
    printf("LR:%p\r\n", (void*) CURRENT_LR);
}

void tip(sysCallArg_t arg)
{
    puts("tips\r\n");
}

void sysCallTest(sysCallArg_t arg)
{
    uint32_t num1 = sys_arg(arg, uint32_t);
    uint32_t num2 = sys_arg(arg, uint32_t);
    uint32_t num3 = sys_arg(arg, uint32_t);
    printf("num1: %d, num2: %d, num3: %d\r\n", num1, num2, num3);
}

uint32_t insts[] = {
    0X2400FFFF,
0X4000000,
0X2400002D,
0X4000000, //
0X24000035,
0X4000000,
0X30000,
// 0X100,
};

uint32_t stack[128];
uint32_t mem[128];

void zCoreStart()
{
    while (1)
    {
        enInst_t inst = ((pInst_t) CURRENT_PC)->inst;
        if (InstHandler[inst] == NULL)
            return;//指令不存在异常

        if (InstHandler[inst]())
            return;//指令执行过程中出现异常
        CURRENT_PC += 4;
    }
}

int main(const int argc, const char** argv)
{
    CoreReg.SVC_Table[1] = printCoreReg;//中断向量1,打印全部寄存器
    CoreReg.SVC_Table[2] = tip;//中断向量2,打印字符串"tip"
    CoreReg.SVC_Table[3] = sysCallTest;//中断向量3,参数传递测试
    // sysCallTest(1, 2, 3, 4, 5);
    //上电复位所有CPU寄存器
    CoreReg.R[INDEX_SP] = (size_t) &stack[127];
    CURRENT_PC = (size_t) insts;//程序入口点
    CoreReg.PSR.Value = 0;
    for (int i = 0;i < 12;i++)
        CoreReg.R[i] = 0;
    CURRENT_DS = (size_t) mem;


    zCoreStart();

    return 0;
}
