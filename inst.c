#include "inst.h"
#include "zCore.h"
#include "sysCallArg.h"
//特殊寄存器下标


InstHanlder_t InstHandler[INST_RET + 1] = {
    //调用
    InstCallHandler,

    //数据存取
    InstPushHandler,
    InstPopHandler,
    InstLdwHandler,
    InstLdhHandler,
    InstLdbHandler,
    InstStwHandler,
    InstSthHandler,
    InstStbHandler,

    //寄存器数据存取
    InstLrlHandler,
    InstLrhHandler,
    InstMovHandler,

    //数学运算
    InstAddHandler,
    InstSubHandler,
    InstMulHandler,
    InstDivHandler,
    InstModHandler,

    //逻辑运算
    InstAndHandler,
    InstOrHandler,
    InstNotHandler,
    InstXorHandler,
    InstEQHandler,
    InstGTHandler,
    InstLTHandler,

    //位运算
    InstBAndHandler,
    InstBOrHandler,
    InstBNotHandler,
    InstBXorHandler,

    //跳转
    InstJHandler,
    InstJTHandler,
    InstJFHandler,
    InstRetHandler,
};

//调用
uint8_t InstCallHandler()
{
    CURRENT_INST(pInstCall_t, pInst);

    uint32_t segNum = pInst->segNum;
    uint8_t srvNum = pInst->srvNumber;

    if (segNum > sizeof(CoreReg.SVC_Table) / sizeof(*CoreReg.SVC_Table))
        return 1;//系统调用异常

    SysCall_t SysCall = CoreReg.SVC_Table[segNum];

    if (SysCall == NULL)
        return 1;//系统调用异常
    
    SysCall((sysCallArg_t) (CURRENT_SP + 4));//执行系统调用
    return 0;
}

//数据存取
uint8_t InstPushHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    *((uint32_t*) CURRENT_SP) = CoreReg.R[pInst->dstReg];
    CURRENT_SP -= 4;
    return 0;
}

uint8_t InstPopHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    CURRENT_SP += 4;
    CoreReg.R[pInst->dstReg] = *((uint32_t*) CURRENT_SP);
    return 0;
}

uint8_t InstLdwHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    if ((size_t) pData % 4)
        return 1;//未对齐访问异常
    CoreReg.R[pInst->dstReg] = *((uint32_t*) pData);
    return 0;
}

uint8_t InstLdhHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    if ((size_t) pData % 2)
        return 1;//未对齐访问异常
    CoreReg.R[pInst->dstReg] = *((uint16_t*) pData);
    return 0;
}

uint8_t InstLdbHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    CoreReg.R[pInst->dstReg] = *((uint8_t*) pData);
    return 0;
}

uint8_t InstStwHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    if ((size_t) pData % 4)
        return 1;//未对齐访问异常
    *((uint32_t*) pData) = CoreReg.R[pInst->dstReg];
    return 0;
}

uint8_t InstSthHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    if ((size_t) pData % 2)
        return 1;//未对齐访问异常
    *((uint16_t*) pData) = CoreReg.R[pInst->dstReg];
    return 0;
}

uint8_t InstStbHandler()
{
    CURRENT_INST(pInstMem_t, pInst);
    void* pData = (void*)(CoreReg.R[pInst->srcReg] + pInst->offset);
    *((uint8_t*) pData) = CoreReg.R[pInst->dstReg];
    return 0;
}

//寄存器数据存取
uint8_t InstLrlHandler()
{
    CURRENT_INST(pInstLrx_t, pInst);
    uint8_t regNum = pInst->regNum;

    CoreReg.R[regNum] &= ~LRL_DATA_MASK;
    CoreReg.R[regNum] |= pInst->data & LRL_DATA_MASK;
    return 0;
}

uint8_t InstLrhHandler()
{
    CURRENT_INST(pInstLrx_t, pInst);
    uint8_t regNum = pInst->regNum;

    CoreReg.R[regNum] &= ~LRH_DATA_MASK;
    CoreReg.R[regNum] |= pInst->data << 16 & LRH_DATA_MASK;
    return 0;
}

uint8_t InstMovHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src = pInst->srcReg0;

    CoreReg.R[dst] = CoreReg.R[src];
    return 0;
}

//数学运算
uint8_t InstAddHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] + CoreReg.R[src1];
    return 0;
}

uint8_t InstSubHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] - CoreReg.R[src1];
    return 0;
}

uint8_t InstMulHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] * CoreReg.R[src1];
    return 0;
}

uint8_t InstDivHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] / CoreReg.R[src1];
    return 0;
}

uint8_t InstModHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] % CoreReg.R[src1];
    return 0;
}

//逻辑运算
uint8_t InstAndHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] && CoreReg.R[src1];
    return 0;
}

uint8_t InstOrHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] || CoreReg.R[src1];
    return 0;
}

uint8_t InstNotHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;

    CoreReg.R[dst] = !CoreReg.R[src0];
    return 0;
}

uint8_t InstXorHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = (CoreReg.R[src0] != 0) ^ (CoreReg.R[src1] != 0);
    return 0;
}

uint8_t InstEQHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.PSR.C = CoreReg.R[src0] == CoreReg.R[src1];
    return 0;
}

uint8_t InstGTHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.PSR.C = CoreReg.R[src0] > CoreReg.R[src1];
    return 0;
}

uint8_t InstLTHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.PSR.C = CoreReg.R[src0] < CoreReg.R[src1];
    return 0;
}

//位运算
uint8_t InstBAndHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] & CoreReg.R[src1];
    return 0;
}

uint8_t InstBOrHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] | CoreReg.R[src1];
    return 0;
}

uint8_t InstBNotHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;

    CoreReg.R[dst] = ~CoreReg.R[src0];
    return 0;
}

uint8_t InstBXorHandler()
{
    CURRENT_INST(pInstCalc_t, pInst);
    uint8_t dst = pInst->dstReg;
    uint8_t src0 = pInst->srcReg0;
    uint8_t src1 = pInst->srcReg1;

    CoreReg.R[dst] = CoreReg.R[src0] ^ CoreReg.R[src1];
    return 0;
}

//跳转
uint8_t InstJHandler()
{
    CURRENT_INST(pInstJmp_t, pInst);
    // int32_t offset = pInst->offset * (-1 * pInst->sign) - 1;//执行完指令会固定+1,所以这里要-1
    int32_t offset = (pInst->sign ? -pInst->offset : pInst->offset) - 1;//执行完指令会固定+1,所以这里要-1
    CURRENT_LR = (size_t) CURRENT_PC;
    CURRENT_PC += offset;
    return 0;
}

uint8_t InstJTHandler()
{
    if (CoreReg.PSR.C)
    {
        CURRENT_INST(pInstJmp_t, pInst);
        int32_t offset = (pInst->sign ? -pInst->offset : pInst->offset) - 1;//执行完指令会固定+1,所以这里要-1
        CURRENT_LR = (size_t) CURRENT_PC;
        CURRENT_PC += offset;
    }
    return 0;
}

uint8_t InstJFHandler()
{
    if (!CoreReg.PSR.C)
    {
        CURRENT_INST(pInstJmp_t, pInst);
        int32_t offset = (pInst->sign ? -pInst->offset : pInst->offset) - 1;//执行完指令会固定+1,所以这里要-1
        CURRENT_LR = (size_t) CURRENT_PC;
        CURRENT_PC += offset;
    }
    return 0;
}

uint8_t InstRetHandler()
{
    CURRENT_PC =  CURRENT_LR - 4;
    return 0;
}
